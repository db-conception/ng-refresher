# Ng Refresher

Get started with angular.
Course of Maximilian Schwarzmüller (https://pro.academind.com/courses/enrolled/766915)

# Notes

## Property binding

```html
<component [property]="value"></component>
```
Here, value is a js expresion. So a variable name, a function call or others js expression

### Custom property
For custom property, we need to add Input() before a property typescript side
```typescript
@Input()
property : string[];
```

## Event binding

```html
<component (event)="click()"></component>
```

### Custom Event
```typescript
@Output() personCreate = new EventEmitter<string>();

onCreatePerson(input : string){
    // Somewhere in your component, you emit value to the event
    this.personCreate.emit(input);
}
```

## String interpolation

```html
<component>{{ stringVariable }}</component>
```

## Two way binding

```html
<component [(ngModel)]="variable"></component>
```
